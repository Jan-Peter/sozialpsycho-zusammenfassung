\documentclass[10pt,landscape]{scrartcl}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% imports
\usepackage{multicol}
\usepackage{calc}
\usepackage{ifthen}
\usepackage[landscape]{geometry}
\usepackage{amsmath,amsthm,amsfonts,amssymb}
\usepackage{hyperref}
\usepackage[utf8x]{inputenc} % utf8x for unix, ansinew for windows
\usepackage[ngerman]{babel}
\usepackage{mathtools}
\usepackage{latexsym}
\usepackage[T1]{fontenc}
\usepackage{cite}
\usepackage[pdftex]{graphicx,color,overpic}
\usepackage{subcaption}
\usepackage{fancyhdr}
\usepackage{tabularx}
\usepackage{ctable}
\usepackage{array}
\usepackage{lscape} % landscape mode
\usepackage{fancyvrb} % fancy verbatim
\usepackage[german]{varioref}
\usepackage{paralist}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\pdfinfo{
  /Title (cheat_sheet.pdf)
  /Creator (TeX)
  /Producer (pdfTeX 1.40.0)
  /Author (Sebastian Schöner)
  /Subject (Cheat sheet)
  /Keywords (pdflatex,latex,pdftex,tex)}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% cheat sheet specifics

% This sets page margins to .5 inch if using letter paper, and to 1cm
% if using A4 paper. (This probably isn't strictly necessary.)
% If using another size paper, use default 1cm margins.
\ifthenelse{\lengthtest { \paperwidth = 11in}}
    { \geometry{top=.5in,left=.5in,right=.5in,bottom=.5in} }
    {\ifthenelse{ \lengthtest{ \paperwidth = 297mm}}
        {\geometry{top=1cm,left=1cm,right=1cm,bottom=1cm} }
        {\geometry{top=1cm,left=1cm,right=1cm,bottom=1cm} }
    }

% Turn off header and footer
\pagestyle{empty}

% Redefine section commands to use less space
\makeatletter
\renewcommand{\section}{\@startsection{section}{1}{0mm}%
                                {-1ex plus -.5ex minus -.2ex}%
                                {0.5ex plus .2ex}%x
                                {\normalfont\large\bfseries}}
\renewcommand{\subsection}{\@startsection{subsection}{2}{0mm}%
                                {-1explus -.5ex minus -.2ex}%
                                {0.5ex plus .2ex}%
                                {\normalfont\normalsize\bfseries}}
\renewcommand{\subsubsection}{\@startsection{subsubsection}{3}{0mm}%
                                {-1ex plus -.5ex minus -.2ex}%
                                {1ex plus .2ex}%
                                {\normalfont\small\bfseries}}
\makeatother

% Define BibTeX command
\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}

% Don't print section numbers
\setcounter{secnumdepth}{0}


\setlength{\parindent}{0pt}
\setlength{\parskip}{0pt plus 0.5ex}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Some custom commands to make life easier
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\du}{\hspace{0pt}} % dummy command
\newcommand{\term}[1]{\textbf{#1}}

% general math commands
\renewcommand{\:}{\ensuremath{\;\;}}
\newcommand{\ds}{\displaystyle}
\newcommand{\lp}{\ensuremath{\left}}
\newcommand{\rp}{\ensuremath{\right}}
\newcommand{\abs}[1]{\ensuremath{\lp|#1\rp|}}
\newcommand{\limv}[1]{\ensuremath{\lim\limits_{#1 \to \infty}}}
\newcommand{\limes}[1]{\ensuremath{\lim\limits_{#1}}}

% commands for various types of sets
\newcommand{\set}[1]{\lp\{#1\rp\}}
\newcommand{\rationaln}{\ensuremath{\mathbb{Q}}}
\newcommand{\realn}{\ensuremath{\mathbb{R}}}
\newcommand{\complexn}{\ensuremath{\mathbb{C}}}
\newcommand{\naturaln}{\ensuremath{\mathbb{N}}}
\newcommand{\naturalnz}{\ensuremath{\mathbb{N}_0}}
\newcommand{\integers}{\ensuremath{\mathbb{Z}}}

% relations (with text, smashed operators)
\renewcommand{\implies}[1][]{\ensuremath{\overset{\overset{#1}{}}{\Rightarrow}}}
\newcommand{\impliest}[1][]{\ensuremath{\overset{\overset{\text{#1}}{}}{
\Rightarrow}}}
\newcommand{\eq}[1][]{\ensuremath{\overset{#1}{=}}}
\newcommand{\eqt}[1]{\ensuremath{\overset{\text{#1}}{=}}}
\newcommand{\leqt}[1]{\ensuremath{\overset{\text{#1}{\leq}}}}
\newcommand{\geqt}[1]{\ensuremath{\overset{\text{#1}{\geq}}}}
\newcommand{\gt}{\ensuremath{>}}
\newcommand{\lt}{\ensuremath{<}}
\newcommand{\smleqt}[1]{\ensuremath{\smashoperator{\mathop{\leq}^{\text{#1}}}}}
\newcommand{\smeq}[1]{\ensuremath{\smashoperator{\mathop{=}^{#1}}}}
\newcommand{\smeqt}[1]{\ensuremath{\smashoperator{\mathop{=}^{\text{#1}}}}}
\newcommand{\smequivt}[1]{\ensuremath{\smashoperator{\mathop{\equiv}^{\text{#1}}
}}}
\newcommand{\smgeqt}[1]{\ensuremath{\smashoperator{\mathop{\geq}^{\text{#1}}}}}
\newcommand{\smgt}[1]{\ensuremath{\smashoperator{\mathop{>}^{\text{#1}}}}}
\newcommand{\smlt}[1]{\ensuremath{\smashoperator{\mathop{<}^{\text{#1}}}}}
\newcommand{\smimpt}[1]{\ensuremath{\smashoperator{\mathop{\implies}^{\text{#1}}
}}}

% proper table rule
\newcommand{\otoprule}{\midrule[\heavyrulewidth]}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% custom environments

% alignment for equations and other math stuff
\newenvironment{eqalign}[1][1]{\[\begin{aligned}}{\end{aligned}\]}

% enumeration with smaller skips
\newenvironment{tightenum}[1][1]
    {\begin{enumerate}[#1]
    \setlength{\itemsep}{2pt}
    \setlength{\parskip}{0pt}
    \setlength{\parsep}{0pt}}
    {\end{enumerate}}
    
% itemize with smaller skips
\newenvironment{tightitemize}
    {\begin{itemize}
    \setlength{\itemsep}{2pt}
    \setlength{\parskip}{0pt}
    \setlength{\parsep}{0pt}}
    {\end{itemize}}
   
\newcounter{studycounter}
\newenvironment{study}[2]
  {
    \stepcounter{studycounter}
    \arabic{studycounter}. \small{\textbf{#1}} \footnotesize{(#2)}\\
  }
  {
    \vspace{-0.3cm}
    \begin{center} \rule{\linewidth}{0.25pt} \end{center}
  }

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
\raggedright
\footnotesize

% use multicols* to avoid auto balancing of columns
\begin{multicols*}{3}

% multicol parameters
% These lengths are set only within the two main columns
%\setlength{\columnseprule}{0.25pt}
\setlength{\premulticols}{1pt}
\setlength{\postmulticols}{1pt}
\setlength{\multicolsep}{1pt}
\setlength{\columnsep}{2pt}

\begin{center}
     \Large{\underline{Sozialpsychologie}} \\
\end{center}

\input{slides1.tex}
\input{slides2.tex}
\input{slides3.tex}
\input{slides4.tex}
\input{slides5.tex}
\input{slides6.tex}
\input{slides7.tex}
\input{slides8.tex}
\input{slides9.tex}
\input{slides10.tex}
\input{slides11.tex}
\input{slides12a.tex}
\input{slides12b.tex}
\input{slides13.tex}
\input{slides14.tex}

\rule{0.3\linewidth}{0.25pt}\\
Jan-Peter Hohloch \& Sebastian Schöner, \today

\end{multicols*}

\end{document}
